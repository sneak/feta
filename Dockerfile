FROM golang:1.14 as builder

WORKDIR /go/src/git.eeqj.de/sneak/feta
COPY . .

#RUN make lint && make build
RUN make build

WORKDIR /go
RUN tar cfz go-src.tgz src && du -sh *

# this container doesn't do anything except hold the build artifact
# and make sure it compiles.

FROM alpine

# here are the levers
ENV FETA_HOSTDISCOVERYPARALLELISM 20
ENV FETA_FSSTORAGELOCATION /state/tootstore
ENV FETA_DBSTORAGELOCATION /state/feta.state.sqlite3
ENV FETA_TOOTSTODISK false
ENV FETA_TOOTSTODB true
ENV FETA_DEBUG false

COPY --from=builder /go/src/git.eeqj.de/sneak/feta/feta /bin/feta
RUN mkdir /app && mkdir /state
COPY --from=builder /go/src/git.eeqj.de/sneak/feta/view /app/view

# put the source in there too for safekeeping
COPY --from=builder /go/go-src.tgz /usr/local/src/go-src.tgz

VOLUME /state
WORKDIR /app

CMD /bin/feta

# FIXME add testing
