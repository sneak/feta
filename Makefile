VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDTIMEFILENAME := $(shell date -u '+%Y%m%d-%H%M%SZ')
BUILDTIMETAG := $(shell date -u '+%Y%m%d%H%M%S')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

FN := feta
IMAGENAME := sneak/$(FN)

UNAME_S := $(shell uname -s)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)

# osx can't statically link apparently?!
ifeq ($(UNAME_S),Darwin)
	GOFLAGS := -ldflags "$(GOLDFLAGS)"
endif

ifneq ($(UNAME_S),Darwin)
	GOFLAGS = -ldflags "-linkmode external -extldflags -static $(GOLDFLAGS)"
endif

default: build

debug: build
	GOTRACEBACK=all FETA_DEBUG=1 ./$(FN)

run: build
	./$(FN)

clean:
	-rm ./$(FN)

build: ./$(FN)

.lintsetup:
	go get -v -u golang.org/x/lint/golint
	go get -u github.com/GeertJohan/fgt
	touch .lintsetup

lint: fmt .lintsetup
	fgt golint ./...

go-get:
	cd cmd/$(FN) && go get -v

./$(FN): */*.go cmd/*/*.go go-get
	cd cmd/$(FN) && go build -o ../../$(FN) $(GOFLAGS) .

fmt:
	gofmt -s -w .

test: lint build-docker-image

is_uncommitted:
	git diff --exit-code >/dev/null 2>&1

build-docker-image: clean
	docker build -t $(IMAGENAME) .

build-docker-image-dist: is_uncommitted clean
	docker build -t $(IMAGENAME):$(VERSION) -t $(IMAGENAME):latest -t $(IMAGENAME):$(BUILDTIMETAG) .

dist: lint build-docker-image
	-mkdir -p ./output
	docker run --rm --entrypoint cat $(IMAGENAME) /bin/$(FN) > output/$(FN)
	docker save $(IMAGENAME) | bzip2 > output/$(BUILDTIMEFILENAME).$(FN).tbz2

hub: upload-docker-image

upload-docker-image: build-docker-image
	docker push $(IMAGENAME):$(VERSION)
	docker push $(IMAGENAME):$(BUILDTIMETAG)
	docker push $(IMAGENAME):latest

.PHONY: build fmt test is_uncommitted build-docker-image dist hub upload-docker-image clean run rundebug default build-docker-image-dist
