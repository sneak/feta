# feta

archives the fediverse

# todo

* scan toots for mentions and feed to locator
* put toots in a separate db file
* test with a real database
* save instancelist to store more often (maybe on each new one added not
  during initial load)
* verify instances load properly on startup
* do some simple in-memory dedupe for toot storage
* make some templates using pongo2 and a simple website
* update json APIs
* index hashtags
* index seen urls

# status

[![Build Status](https://drone.datavi.be/api/badges/sneak/feta/status.svg)](https://drone.datavi.be/sneak/feta)

# ethics statement

It seems that some splinter groups are not well acquainted with the norms of
publishing data on the web.

Publishing your toots/messages on a server without marking them private or
requiring authentication and thus making them available to the web is an act
of affirmative consent to allowing others to download those toots/messages
(usually by viewing them in a browser on your profile page).  If you don't
want your toots downloaded by remote/unauthenticated users on the web, do
not publish them to the web.

If you publish them to the whole web (and your home instance serves them to
all comers), do not be surprised or feel violated when people download (and
optionally save) them, as your home instance permits them to.

We do not have a right to be forgotten, as we do not have a right to delete
legitimately-obtained files from the hard drives of other people.

# Author

Jeffrey Paul &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;

[@sneak@sneak.berlin](https://s.sneak.berlin/@sneak)
