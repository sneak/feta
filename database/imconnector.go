package database

import (
	"git.eeqj.de/sneak/feta/instance"
	"github.com/rs/zerolog/log"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func (m *Manager) SaveInstance(i *instance.Instance) error {
	i.Lock()
	defer i.Unlock()
	var x APInstance
	if m.db.Where("UUID = ?", i.UUID).First(&x).RecordNotFound() {
		log.Info().
			Str("hostname", i.Hostname).
			Msg("instance not in db, inserting")
		// item does not exist in db yet, must insert
		ni := APInstance{
			UUID:                       i.UUID,
			Disabled:                   i.Disabled,
			ErrorCount:                 i.ErrorCount,
			FSMState:                   i.Status(),
			Fetching:                   i.Fetching,
			HighestID:                  i.HighestID,
			Hostname:                   i.Hostname,
			Identified:                 i.Identified,
			Implementation:             i.Implementation,
			NextFetch:                  i.NextFetch,
			LastError:                  i.LastError,
			NodeInfoURL:                i.NodeInfoURL,
			ServerImplementationString: i.ServerImplementationString,
			ServerVersionString:        i.ServerVersionString,
			SuccessCount:               i.SuccessCount,
		}
		r := m.db.Create(&ni)
		return r.Error
	} else {
		log.Info().
			Str("hostname", i.Hostname).
			Str("id", i.UUID.String()).
			Msg("instance found in db, updating")
		// exists in db, update db
		var ei APInstance
		// EI EI uh-oh
		m.db.Where("UUID = ?", i.UUID).First(&ei)
		ei.Disabled = i.Disabled
		ei.ErrorCount = i.ErrorCount
		ei.FSMState = i.Status()
		ei.Fetching = i.Fetching
		ei.HighestID = i.HighestID
		ei.Hostname = i.Hostname
		ei.LastError = i.LastError
		ei.Identified = i.Identified
		ei.Implementation = string(i.Implementation)
		ei.NextFetch = i.NextFetch
		ei.NodeInfoURL = i.NodeInfoURL
		ei.ServerImplementationString = i.ServerImplementationString
		ei.ServerVersionString = i.ServerVersionString
		ei.SuccessCount = i.SuccessCount
		r := m.db.Save(&ei)
		return r.Error
	}
}

func (m *Manager) ListInstances() ([]*instance.Instance, error) {
	output := make([]*instance.Instance, 0)

	var results []APInstance
	m.db.Find(&results)

	for _, i := range results {
		newinst := instance.New(func(x *instance.Instance) {
			x.UUID = i.UUID
			x.Disabled = i.Disabled
			x.ErrorCount = i.ErrorCount
			x.InitialFSMState = i.FSMState
			x.Fetching = i.Fetching
			x.HighestID = i.HighestID
			x.Hostname = i.Hostname
			x.LastError = i.LastError
			x.Identified = i.Identified
			x.Implementation = i.Implementation
			x.NextFetch = i.NextFetch
			x.NodeInfoURL = i.NodeInfoURL
			x.ServerImplementationString = i.ServerImplementationString
			x.ServerVersionString = i.ServerVersionString
			x.SuccessCount = i.SuccessCount
		})
		output = append(output, newinst)
	}

	return output, nil
}
