package database

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type StoredToot struct {
	gorm.Model
	UUID uuid.UUID `gorm:"type:uuid;primary_key;"`
	//Original      string    `sql:"type:text"`
	Original      []byte
	Hash          string `gorm:"unique_index"`
	ServerCreated time.Time
	Acct          string
	Content       []byte
	TextContent   []byte
	URL           string
	Hostname      string `gorm:"index:hostnameindex"`
}

type APInstance struct {
	gorm.Model
	UUID                       uuid.UUID `gorm:"type:uuid;primary_key;"`
	ErrorCount                 uint
	SuccessCount               uint
	HighestID                  uint
	Hostname                   string `gorm:"type:varchar(100);unique_index"`
	Identified                 bool
	Fetching                   bool
	Disabled                   bool
	LastError                  string
	Implementation             string
	NextFetch                  time.Time
	NodeInfoURL                string
	ServerVersionString        string
	ServerImplementationString string
	FSMState                   string
}

// NB that when you add a model below you must add it to this list!
func (m *Manager) doMigrations() {
	log.Info().Msg("doing database migrations if required")
	m.db.AutoMigrate(&APInstance{})
	m.db.AutoMigrate(&StoredToot{})
}
