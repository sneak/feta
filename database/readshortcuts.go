package database

import (
	"github.com/google/uuid"
)

func (m *Manager) TootCountForHostname(hostname string) (uint, error) {
	var c uint
	e := m.db.Model(&StoredToot{}).Where("hostname = ?", hostname).Count(&c)
	if e.Error != nil {
		return 0, e.Error
	} else {
		return c, nil
	}
}

func (m *Manager) GetAPInstanceFromUUID(uuid *uuid.UUID) (*APInstance, error) {
	var i APInstance
	e := m.db.Model(&APInstance{}).Where("uuid = ?", uuid).First(&i)
	if e.Error != nil {
		return nil, e.Error
	}
	return &i, nil
}
