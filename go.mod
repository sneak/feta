module git.eeqj.de/sneak/feta

go 1.14

require (
	git.eeqj.de/sneak/goutil v0.0.0-20200330224956-7fad5dc142e5
	github.com/flosch/pongo2 v0.0.0-20190707114632-bbf5a6c351f4
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.1
	github.com/grokify/html-strip-tags-go v0.0.0-20200322061010-ea0c1cf2f119
	github.com/jinzhu/gorm v1.9.12
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/looplab/fsm v0.1.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-isatty v0.0.12
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/mayowa/echo-pongo2 v0.0.0-20170410154925-661ce95e1767
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/multiformats/go-multihash v0.0.13
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/ziflex/lecho v1.2.0
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
	github.com/golang/groupcache v0.0.0-20190129154638-5b532d6fd5ef
)
