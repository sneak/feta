package jsonapis

import "fmt"
import "encoding/json"

func (atl *apTootList) String() string {
	return fmt.Sprintf("%+v", atl)
}

type apTootList []json.RawMessage
