package jsonapis

import "time"

// thank fuck for https://mholt.github.io/json-to-go/ otherwise
// this would have been a giant pain in the dick

// MastodonIndexResponse is the json api shape from the mastodon instance
// indexer
type MastodonIndexResponse struct {
	Instances []struct {
		ID                 string      `json:"_id"`
		AddedAt            time.Time   `json:"addedAt"`
		Name               string      `json:"name"`
		Downchecks         int         `json:"downchecks"`
		Upchecks           int         `json:"upchecks"`
		HTTPSRank          interface{} `json:"https_rank"`
		HTTPSScore         int         `json:"https_score"`
		ObsRank            string      `json:"obs_rank"`
		ObsScore           int         `json:"obs_score"`
		Ipv6               bool        `json:"ipv6"`
		Up                 bool        `json:"up"`
		Users              int         `json:"users"`
		Statuses           string      `json:"statuses"`
		Connections        int         `json:"connections"`
		OpenRegistrations  bool        `json:"openRegistrations"`
		Uptime             float64     `json:"uptime"`
		Version            string      `json:"version"`
		VersionScore       int         `json:"version_score"`
		UpdatedAt          time.Time   `json:"updatedAt"`
		CheckedAt          time.Time   `json:"checkedAt"`
		Dead               bool        `json:"dead"`
		ObsDate            time.Time   `json:"obs_date"`
		Second60           int         `json:"second60"`
		Second             int         `json:"second"`
		ActiveUserCount    interface{} `json:"active_user_count,omitempty"`
		FirstUserCreatedAt interface{} `json:"first_user_created_at,omitempty"`
		Thumbnail          string      `json:"thumbnail"`
		ApUpdatedAt        time.Time   `json:"apUpdatedAt"`
		Second5            int         `json:"second5"`
		RawVersion         string      `json:"raw_version"`
		ActivityPrevw      struct {
			Statuses      int `json:"statuses"`
			Logins        int `json:"logins"`
			Registrations int `json:"registrations"`
		} `json:"activity_prevw,omitempty"`
		Mastodon  bool   `json:"mastodon"`
		UptimeStr string `json:"uptime_str"`
		Score     int    `json:"score"`
		ScoreStr  string `json:"score_str"`
	} `json:"instances"`
}

// PleromaIndexResponse is the json api shape from the pleroma instance
// indexer
type PleromaIndexResponse []struct {
	Domain                    string `json:"domain"`
	Title                     string `json:"title"`
	Thumbnail                 string `json:"thumbnail"`
	Registration              bool   `json:"registration"`
	Chat                      bool   `json:"chat"`
	Gopher                    bool   `json:"gopher"`
	WhoToFollow               bool   `json:"who_to_follow"`
	MediaProxy                bool   `json:"media_proxy"`
	ScopeOptions              bool   `json:"scope_options"`
	AccountActivationRequired bool   `json:"account_activation_required"`
	TextLimit                 int    `json:"text_limit"`
}

// NodeInfoVersionTwoSchema is the json format of nodeinfo 2.0
type NodeInfoVersionTwoSchema struct {
	Version  string `json:"version"`
	Software struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"software"`
	Protocols []string `json:"protocols"`
	Usage     struct {
		Users struct {
			Total          int `json:"total"`
			ActiveMonth    int `json:"activeMonth"`
			ActiveHalfyear int `json:"activeHalfyear"`
		} `json:"users"`
		LocalPosts int `json:"localPosts"`
	} `json:"usage"`
	OpenRegistrations bool `json:"openRegistrations"`
}

// NodeInfoWellKnownResponse is the json format of the nodeinfo schema
type NodeInfoWellKnownResponse struct {
	Links []struct {
		Rel  string `json:"rel"`
		Href string `json:"href"`
	} `json:"links"`
}

// APISerializedToot is a partial shape of the json serialized form of a
// toot from the mastodon api (also used by pleroma).  We save the original
// json from the server though so this is just a minimal subset that we need
// to deserialize for purposes of this indexer app.
type APISerializedToot struct {
	Account struct {
		Acct     string `json:"acct"`
		ID       string `json:"id"`
		URL      string `json:"url"`
		Username string `json:"username"`
	} `json:"account"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	ID        string    `json:"id"`
	Mentions  []struct {
		Acct     string `json:"acct"`
		ID       string `json:"id"`
		URL      string `json:"url"`
		Username string `json:"username"`
	} `json:"mentions"`
	URI string `json:"uri"`
	URL string `json:"url"`
}
