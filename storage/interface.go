package storage

import (
	"git.eeqj.de/sneak/feta/toot"
)

// TootStorageBackend is the interface to which storage backends must
// conform for storing toots
type TootStorageBackend interface {
	TootExists(t *toot.Toot) bool
	StoreToot(t *toot.Toot) error
	StoreToots(tc []*toot.Toot) error
}
